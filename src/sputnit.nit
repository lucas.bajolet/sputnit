module sputnit is
	app_name "Sputnit"
	android_manifest_activity """android:screenOrientation="sensorLandscape""""
end

import gamnit::depth
import gamnit::keys

redef class App

	#var model_player = new Model("models/male.obj")
	#var grass = new Texture("grass_top.png")

	var dt = 0.0

	redef fun on_create
	do
		super

		for model in models do model.load
		#for texture in root_textures do texture.load

		var a = new Actor(placeholder_model, new Point3d[Float](0.0, 0.0, 0.0))
		actors.add a
	end

	redef fun update(dt)
	do
		self.dt += dt

		for key in app.pressed_keys do
			if key == "w" then
			else if key == "s" then
			else if key == "a" then
			else if key == "d" then
			end
		end
	end

	redef fun accept_event(event)
	do
		super

		if event isa KeyEvent and event.name == "escape" then
			exit 0
		else if event isa KeyEvent and event.is_down then
			var key = event.name
			if key == "up" then
			else if key == "down" then
			else if key == "space" then
			end
		end

		return false
	end
end
