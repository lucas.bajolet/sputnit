NIT_DIR ?= ~/projects/nit/

bin/sputnit: $(shell nitls -M src/sputnit.nit -m linux)
	nitc -o $@ src/sputnit.nit -m linux
